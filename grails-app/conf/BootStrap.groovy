import uk.ac.leedsBeckett.Reading

class BootStrap {

    def init = { servletContext ->
        new Reading(date: new Date(2017, 0, 1), meterReading: 100000, houseNameOrNumber: "The Grange", postcode: "LS6 3QX").save(flush: true)
        new Reading(date: new Date(2017, 1, 1), meterReading: 100025, houseNameOrNumber: "The Grange", postcode: "LS6 3QX").save(flush: true)
        new Reading(date: new Date(2017, 2, 1), meterReading: 100075, houseNameOrNumber: "The Grange", postcode: "LS6 3QX").save(flush: true)
    }
    def destroy = {
    }
}
