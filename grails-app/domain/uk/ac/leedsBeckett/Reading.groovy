package uk.ac.leedsBeckett

import grails.rest.*

@Resource(uri='/reading')
class Reading {
    Date date
    Integer meterReading
    String houseNameOrNumber
    String postcode
}
