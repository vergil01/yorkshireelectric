# README #

* This is a very simple RESTful API for managing meter readings for a fictitious company, Yorkshire Electric.

### Versions ###

* Built using Grails 2.5.0 and JDK 8
* Deploys to Tomcat 8

### How do I get set up? ###

* Run the application locally in development mode with 'grails run-app'.
* Or connect to a real database and deploy it to a Tomcat server. 

### Deployment ###

* This application is deployed to Microsoft Azure and connects to a SQL Server database.